
## Get-InstalledWindowsUpdates.ps1

|                  |                                                                                                            |
|------------------|------------------------------------------------------------------------------------------------------------|
| **OS:**          | Windows                                                                                                    |
| **Type:**        | A Windows PowerShell script                                                                                |
| **Language:**    | Windows PowerShell                                                                                         |
| **Description:** | Get-InstalledWindowsUpdates uses Windows Management Instrumentation (WMI, with a command "`Get-WmiObject -Class Win32_QuickFixEngineering`" (Method 1)) to retrieve a list of some HotFixIDs installed on a local machine and displays the results in console. The Windows Management Instrumentation Command-Line Utility (`WMIC.exe` with the query path of `win32_quickfixengineering` (Method 2)) is then used to write a secondary CSV-file (`partial_hotfix_list.csv`) to `$path` – this secondary CSV-file populated by `WMIC.exe` contains about the same partial results as which were obtained by using the previous Method 1.  |
|                  | Finally, Get-InstalledWindowsUpdates uses Windows Update Agent (WUA) API (Method 3) to retrieve a third – comprehensive – list of all the installed and uninstalled Windows updates, and displays those results in a pop-up window and writes them to a CSV-file (`installed_windows_updates.csv`). This script is based on Stéphane van Gulick's PowerShell function "[Get-WindowsUpdates](https://gallery.technet.microsoft.com/Get-WindowsUpdates-06eb7f43)". |
|                  | For further information, please see the [Wiki](https://bitbucket.org/auberginehill/get-installed-windows-updates/wiki/).     |
| **Homepage:**    | https://bitbucket.org/auberginehill/get-installed-windows-updates                                                          |
|                  | Short URL: https://tinyurl.com/y2u9s3xh                                                                    |
| **Version:**     | 2.0                                                                                                        |
| **Downloads:**   | For instance [Get-InstalledWindowsUpdates.ps1](https://bitbucket.org/auberginehill/get-installed-windows-updates/src/master/Get-InstalledWindowsUpdates.ps1). Or [everything as a .zip-file](https://bitbucket.org/auberginehill/get-installed-windows-updates/downloads/). Or `git clone https://auberginehill@bitbucket.org/auberginehill/get-installed-windows-updates.git`. Or `git fetch && git checkout master`. |




### Screenshot

![Screenshot](https://bitbucket.org/auberginehill/get-installed-windows-updates/raw/345d6a843324ee6e15e27d50a0e138dceb33a14a/Get-InstalledWindowsUpdates.png)




### www

|                        |                                                                              |                               |
|:----------------------:|------------------------------------------------------------------------------|-------------------------------|
| :globe_with_meridians: | [Script Homepage](https://github.com/auberginehill/get-installed-windows-updates) |                          |
|                        | [How to List All of the Windows and Software Updates Applied to a Computer](http://social.technet.microsoft.com/wiki/contents/articles/4197.how-to-list-all-of-the-windows-and-software-updates-applied-to-a-computer.aspx) | Microsoft TechNet |
|                        | [How Can I Tell Which Service Packs Have Been Installed on a Computer?](https://blogs.technet.microsoft.com/heyscriptingguy/2004/09/29/how-can-i-tell-which-service-packs-have-been-installed-on-a-computer/) | ScriptingGuy1 |
|                        | [Cannot index into a null array](https://social.technet.microsoft.com/Forums/en-US/99581c8b-4814-4419-8f4b-34f9cfca802b/cannot-index-into-a-null-array?forum=winserverpowershell) | Anna Wang |
|                        | [Get-WindowsUpdates](https://gallery.technet.microsoft.com/Get-WindowsUpdates-06eb7f43) | Stéphane van Gulick |
|                        | [Invoke-WindowsUpdate](https://gallery.technet.microsoft.com/ScriptCenter/d86cd93b-2428-40a1-a430-26bd3caed36f/) | Jan Egil Ring |
|                        | [Manage Windows Update installations using Windows PowerShell](http://blog.powershell.no/2010/06/25/manage-windows-update-installations-using-windows-powershell/) | |
|                        | [Manage Windows Update installations using Windows PowerShell](http://blog.crayon.no/blogs/janegil/archive/2010/06/25/manage_2D00_windows_2D00_update_2D00_installations_2D00_using_2D00_windows_2D00_powershell.aspx) | |
|                        | [Managing Windows Update with PowerShell](https://blogs.technet.microsoft.com/jamesone/2009/01/27/managing-windows-update-with-powershell/) | |
|                        | [Appendix B. Regular Expression Reference](http://powershellcookbook.com/recipe/qAxK/appendix-b-regular-expression-reference) | |
|                        | [How to Use PowerShell to Run Windows Updates](http://www.ehow.com/how_8724332_use-powershell-run-windows-updates.html) | |
|                        | [Using the Windows Update Agent API](https://msdn.microsoft.com/en-us/library/aa387287(v=VS.85).aspx) |      |
|                        | [OperationResultCode enumeration](https://msdn.microsoft.com/en-us/library/windows/desktop/aa387095(v=vs.85).aspx) | |
|                        | [ServerSelection enumeration](https://msdn.microsoft.com/en-us/library/windows/desktop/aa387280(v=vs.85).aspx) | |
|                        | [UpdateOperation enumeration](https://msdn.microsoft.com/en-us/library/windows/desktop/aa387282(v=vs.85).aspx) | |
|                        | [IUpdateHistoryEntry interface](https://msdn.microsoft.com/en-us/library/windows/desktop/aa386400(v=vs.85).aspx) | |
|                        | [The String's the Thing](https://technet.microsoft.com/en-us/library/ee692804.aspx) |                        |
|                        | [Dig Deep into your system with Dedicated System Information Tools in Windows 7](https://technet.microsoft.com/en-us/library/ee835740.aspx) | |
|                        | [Why are "get-hotfix" and "wmic qfe list" in Powershell missing installed updates?](http://superuser.com/questions/1002015/why-are-get-hotfix-and-wmic-qfe-list-in-powershell-missing-installed-updates) | |
|                        | [Why is there a DAILY update for English Input Personalization Dictionary](https://answers.microsoft.com/en-us/windows/forum/windowsrt8_1-windows_update/why-is-there-a-daily-update-for-english-input/83352bfe-86df-4a3b-9886-24c1a9401b78?page=3&msgId=53a108e7-7fef-47e2-87e5-78588d9090ca) | |
|                        | [http://www.figlet.org/](http://www.figlet.org/) and [ASCII Art Text Generator](http://www.network-science.de/ascii/) | ASCII Art |
|                        | [HTML To Markdown Converter](https://digitalconverter.azurewebsites.net/HTML-to-Markdown-converter) |        |
|                        | [Markdown Tables Generator](https://www.tablesgenerator.com/markdown_tables) |                               |
|                        | [HTML table syntax into Markdown](https://jmalarcon.github.io/markdowntables/) |                             |
|                        | [A HTML to Markdown converter written in JavaScript](https://domchristie.github.io/turndown/) | Dom Christie |
|                        | [Paste to Markdown](https://euangoddard.github.io/clipboard2markdown/)       |                               |
|                        | [Convert HTML or anything to Markdown](https://cloudconvert.com/html-to-md)  |                               |
