```

   _____      _        _____           _        _ _          ___          ___           _                   _    _           _       _
  / ____|    | |      |_   _|         | |      | | |        | \ \        / (_)         | |                 | |  | |         | |     | |
 | |  __  ___| |_ ______| |  _ __  ___| |_ __ _| | | ___  __| |\ \  /\  / / _ _ __   __| | _____      _____| |  | |_ __   __| | __ _| |_ ___  ___
 | | |_ |/ _ \ __|______| | | '_ \/ __| __/ _` | | |/ _ \/ _` | \ \/  \/ / | | '_ \ / _` |/ _ \ \ /\ / / __| |  | | '_ \ / _` |/ _` | __/ _ \/ __|
 | |__| |  __/ |_      _| |_| | | \__ \ || (_| | | |  __/ (_| |  \  /\  /  | | | | | (_| | (_) \ V  V /\__ \ |__| | |_) | (_| | (_| | ||  __/\__ \
  \_____|\___|\__|    |_____|_| |_|___/\__\__,_|_|_|\___|\__,_|   \/  \/   |_|_| |_|\__,_|\___/ \_/\_/ |___/\____/| .__/ \__,_|\__,_|\__\___||___/
                                                                                                                  | |
                                                                                                                  |_|            
```

# Get-InstalledWindowsUpdates Wiki

[TOC]

## Screenshot

![Screenshot](https://bitbucket.org/auberginehill/get-installed-windows-updates/raw/345d6a843324ee6e15e27d50a0e138dceb33a14a/Get-InstalledWindowsUpdates.png)




## Outputs 

:arrow_right: Displays a partial list of installed HotFixIDs in console.

  - Displays a list of all installed Windows updates in a pop-up window "`$windows_updates_sorted_selection`" (`Out-GridView`). Also writes two CSV-files at `$path`.

  - A pop-up window (`Out-GridView`):

    | Name                                | Description                                  |
    |-------------------------------------|----------------------------------------------|
    | `$windows_updates_sorted_selection` | Displays a list of installed Windows updates |

  - And also two CSV-files at `$path`.

    | Path                                      | Type     | Name                            |
    |-------------------------------------------|----------|---------------------------------|
    | `$env:temp\partial_hotfix_list.csv`       | CSV-file | `partial_hotfix_list.csv`       |
    | `$env:temp\installed_windows_updates.csv` | CSV-file | `installed_windows_updates.csv` |




## Notes 

:warning: Please note that the files are created in a directory, which is specified with the `$path` variable (at line 10).

  - The `$env:temp` variable points to the current temp folder. The default value of the `$env:temp` variable is `C:\Users\<username>\AppData\Local\Temp` (i.e. each user account has their own separate temp folder at path `%USERPROFILE%\AppData\Local\Temp`). To see the current temp path, for instance a command...

    `[System.IO.Path]::GetTempPath()`

    ...may be used at the PowerShell prompt window `[PS\>]`. 

  - To change the temp folder for instance to `C:\Temp`, please, for example, follow the instructions at [Temporary Files Folder - Change Location in Windows](http://www.eightforums.com/tutorials/23500-temporary-files-folder-change-location-windows.html), which in essence are something along the lines:
    1. Right click Computer icon and select Properties (or select Start → Control Panel → System. On Windows 10 this instance may also be found by right clicking Start and selecting Control Panel → System... or by pressing `[Win-key]` + X and selecting Control Panel → System). On the window with basic information about the computer...
    1. Click on Advanced system settings on the left panel and select Advanced tab on the "System Properties" pop-up window.
    1. Click on the button near the bottom labeled Environment Variables.
    1. In the topmost section, which lists the User variables, both TMP and TEMP may be seen. Each different login account is assigned its own temporary locations. These values can be changed by double clicking a value or by highlighting a value and selecting Edit. The specified path will be used by Windows and many other programs for temporary files. It's advisable to set the same value (a directory path) for both TMP and TEMP.
    1. Any running programs need to be restarted for the new values to take effect. In fact, probably Windows itself needs to be restarted for it to begin using the new values for its own temporary files.




## Examples

:book: To open this code in Windows PowerShell, for instance:

  1. `./Get-InstalledWindowsUpdates`

    Runs the script. Please notice to insert `./` or `.\` before the script name.

  1. `help ./Get-InstalledWindowsUpdates -Full`

    Displays the help file.

  1. `New-Item -ItemType File -Path C:\Temp\Get-InstalledWindowsUpdatess.ps1`

    Creates an empty ps1-file to the `C:\Temp` directory. The `New-Item` cmdlet has an inherent `-NoClobber` mode built into it, so that the procedure will halt, if overwriting (replacing the contents) of an existing file is about to happen. Overwriting a file with the `New-Item` cmdlet requires using the `Force`. If the path name and/or the filename includes space characters, please enclose the whole `-Path` parameter value in quotation marks (single or double): `New-Item -ItemType File -Path "C:\Folder Name\Get-InstalledWindowsUpdates.ps1"`. For more information, please type "`help New-Item -Full`".

  1. `Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope LocalMachine`

    This command is altering the Windows PowerShell rights to enable script execution in the default (`LocalMachine`) scope, and defines the conditions under which Windows PowerShell loads configuration files and runs scripts in general. In Windows Vista and later versions of Windows, for running commands that change the execution policy of the `LocalMachine` scope, Windows PowerShell has to be run with elevated rights (Run as Administrator). The default policy of the default (`LocalMachine`) scope is "`Restricted`", and a command "`Set-ExecutionPolicy Restricted`" will "undo" the changes made with the original example above (had the policy not been changed before...). Execution policies for the local computer (`LocalMachine`) and for the current user (`CurrentUser`) are stored in the registry (at for instance the `HKLM:\Software\Policies\Microsoft\Windows\PowerShell\ExecutionPolicy` key), and remain effective until they are changed again. The execution policy for a particular session (`Process`) is stored only in memory, and is discarded when the session is closed.

    |                | PowerShell Execution Policy Parameters                                         |
    |----------------|--------------------------------------------------------------------------------|
    | `Restricted`   | Does not load configuration files or run scripts, but permits individual commands. `Restricted` is the default execution policy. |
    | `AllSigned`    | Scripts can run. Requires that all scripts and configuration files be signed by a trusted publisher, including the scripts that have been written on the local computer. Risks running signed, but malicious, scripts. |
    | `RemoteSigned` | Requires a digital signature from a trusted publisher on scripts and configuration files that are downloaded from the Internet (including e-mail and instant messaging programs). Does not require digital signatures on scripts that have been written on the local computer. Permits running unsigned scripts that are downloaded from the Internet, if the scripts are unblocked by using the `Unblock-File` cmdlet. Risks running unsigned scripts from sources other than the Internet and signed, but malicious, scripts. |
    | `Unrestricted` | Loads all configuration files and runs all scripts. Warns the user before running scripts and configuration files that are downloaded from the Internet. Not only risks, but actually permits, eventually, runningany unsigned scripts from any source. Risks running malicious scripts. |
    | `Bypass`       | Nothing is blocked and there are no warnings or prompts. Not only risks, but actually permits running any unsigned scripts from any source. Risks running malicious scripts. |
    | `Undefined`    | Removes the currently assigned execution policy from the current scope. If the execution policy in all scopes is set to `Undefined`, the effective execution policy is `Restricted`, which is the default execution policy. This parameter will not alter or remove the ("master") execution policy that is set with a Group Policy setting. |
    | **Notes:**     | Please note that the Group Policy setting "`Turn on Script Execution`" overrides the execution policies set in Windows PowerShell in all scopes. To find this ("master") setting, please, for example, open the Local Group Policy Editor (`gpedit.msc`) and navigate to Computer Configuration → Administrative Templates → Windows Components → Windows PowerShell. The Local Group Policy Editor (`gpedit.msc`) is not available in any Home or Starter edition of Windows. |
    |                | For more information, please type "`Get-ExecutionPolicy -List`", "`help Set-ExecutionPolicy -Full`", "`help about_Execution_Policies`" or visit [Set-ExecutionPolicy](https://technet.microsoft.com/en-us/library/hh849812.aspx) or [About Execution Policies](http://go.microsoft.com/fwlink/?LinkID=135170). |




## Contributing

|        |                           |                                                                                                                        |
|:------:|---------------------------|------------------------------------------------------------------------------------------------------------------------|
| :herb: | **Bugs:**                 | Bugs can be reported by creating a new [issue](https://bitbucket.org/auberginehill/get-installed-windows-updates/issues/).               |
|        | **Feature Requests:**     | Feature request can be submitted by creating a new [issue](https://bitbucket.org/auberginehill/get-installed-windows-updates/issues/).   |
|        | **Editing Source Files:** | New features, fixes and other potential changes can be discussed in further detail by opening a [pull request](https://bitbucket.org/auberginehill/get-installed-windows-updates/pull-requests/). |

For further information, please see the [Contributing.md](https://bitbucket.org/auberginehill/get-installed-windows-updates/wiki/Contributing.md) page.




## Wiki Features

The wiki itself is actually a git repository, which means you can clone it, edit it locally/offline, add images or any other file type, and push it back to us. It will be live immediately.

```
$ git clone https://auberginehill@bitbucket.org/auberginehill/get-installed-windows-updates.git/wiki
```

Wiki pages are normal files, with the .md extension. You can edit them locally, as well as creating new ones.